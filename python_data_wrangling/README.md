# Python Data Wrangling Primer

This workshop segment consists of a brief primer on Python and data wrangling using the Pandas module. The content is entirely contained within the Jupyter notebook called 'data_wrangling.ipynb'.

[**Follow These Instructions to Launch Jupyter Lab on DCC**](misc/jupyter_ondemand_howto.md)

A brief list of topics covered:

- Brief Intro to Python
    + Object types
    + Modules
    + Built-in functions
    + Programming constructions
- Data Wrangling with Pandas
    + Loading data from a file
    + Creating a DataFrame object
    + Modifying columns
    + Slicing/subsetting
    + Regular expressions
    + Modifying values
    + Sorting
    + Combining DataFrames
    + Reshaping DataFrames
    + Exporting a DataFrame object to a file
- Data Exploration
    + Summary statistics
    + Plotting with ggplot via the plotnine module
    + Basic plotting with Pandas .plot methods
