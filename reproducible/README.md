# Reproducible Research
- [Reproducible Research Lecture Notes](reproducible_research_lecture.md)

## Git
- [Overview](git_overview.md)
- [Conflicts](git_conflicts.md)
- [Team Exercise](git_team_exercise.md)
- [Cloning and Forking](git_cloning_and_forking.md)
- [Branching, Stashing, Tagging](branch_stash_tag.md)
